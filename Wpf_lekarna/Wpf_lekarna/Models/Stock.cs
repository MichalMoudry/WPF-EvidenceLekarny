﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Wpf_lekarna.Models
{
    public class Stock : Interfaces.DatabaseTemplate
    {   
        [PrimaryKey, AutoIncrement, Indexed]
        public int ID { get; set; }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _amount;
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

    }
}
