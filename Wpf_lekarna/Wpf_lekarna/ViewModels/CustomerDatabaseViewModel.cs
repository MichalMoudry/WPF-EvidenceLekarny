﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;
using Wpf_lekarna.Interfaces;

namespace Wpf_lekarna.ViewModels
{
    class CustomerDatabaseViewModel : IDatabaseViewModel<Customer>
    {
        private static CustomerDatabaseViewModel _instance;
        public static CustomerDatabaseViewModel Instance()
        {
            if (_instance == null)
            {
                _instance = new CustomerDatabaseViewModel();
            }
            return _instance;
        }

        private static CustomerDatabase _customerDatabase;
        private static CustomerDatabase CustomerDatabase
        {
            get
            {
                if (_customerDatabase == null)
                {
                    var fileHelper = new Services.FileHelper();
                    _customerDatabase = new CustomerDatabase(fileHelper.GetLocalFilePath("customerDatabase.db3"));
                }
                return _customerDatabase;
            }
        }

        public async Task SaveItem(Customer item)
        {
            await CustomerDatabase.SaveItemAsync(item);
        }

        public List<Customer> LoadData()
        {
            return CustomerDatabase.GetItemsAsync<Customer>().Result;
        }

        public async Task DeleteItem(Customer item)
        {
            await CustomerDatabase.DeleteItemAsync(item);
        }

        public async Task<Customer> GetItem(int id)
        {
            var temp = await CustomerDatabase.GetItemAsync<Customer>(id);
            return temp;
        }
    }
}
