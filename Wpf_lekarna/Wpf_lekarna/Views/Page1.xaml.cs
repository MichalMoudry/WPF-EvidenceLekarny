﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf_lekarna.Models;
using Wpf_lekarna.ViewModels;
using System.Windows.Media.Effects;
using System.Collections.ObjectModel;

namespace Wpf_lekarna.Views
{
    /// <summary>
    /// Interakční logika pro Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {
        public Page1()
        {
            InitializeComponent();

            var orderJoin = from customersDatabase in CustomerDatabaseViewModel.Instance().LoadData()
                            join ordersDatabase in OrderDatabaseViewModel.Instance().LoadData() on customersDatabase.ID equals ordersDatabase.CustomerID
                            select new { CustomerName = customersDatabase.Name, OrderID = ordersDatabase.ID, OrderDate = ordersDatabase.Date };
            orderListView.ItemsSource = orderJoin;

            customerObsColl = new ObservableCollection<Customer>(
                CustomerDatabaseViewModel.Instance().LoadData().OrderBy(i => i.ID)
            );
            customerListView.ItemsSource = customerObsColl;
            drugObsColl = new ObservableCollection<Drug>(
                DrugDatabaseViewModel.Instance().LoadData().OrderBy(i => i.ID)
            );

            drugListView.ItemsSource = drugObsColl;

            blEffect.Radius = 0;
            content.Effect = blEffect;

            userDOBInput.DisplayDateEnd = DateTime.Now;
            DataToPurchasingDrugs();
            //customerOrdersList.Text = $"Objednávky:\n";
        }

        //Variables and instances
        Customer customer;
        Drug drug;
        Order order;
        Stock stock;
        BlurEffect blEffect = new BlurEffect();

        ObservableCollection<Customer> customerObsColl;
        ObservableCollection<Drug> drugObsColl;
        
        int num;
        bool numCheck1;
        bool numCheck2;
        bool numCheck3;
        bool numCheck4;
        int tempSelectedIndex;

        //Adding methods
        private async void addCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            numCheck1 = int.TryParse(userNameInput.Text, out num);
            numCheck2 = int.TryParse(userAllergensInput.Text, out num);
            if (string.IsNullOrEmpty(userNameInput.Text).Equals(false) && numCheck1.Equals(false) && numCheck2.Equals(false) && string.IsNullOrEmpty($"{userDOBInput.SelectedDate}").Equals(false))
            {
                customer = new Customer();
                customer.Name = userNameInput.Text;
                customer.Allergens += userAllergensInput.Text;
                customer.Added = DateTime.Now;
                customer.DateOfBirth = $"{userDOBInput.SelectedDate}";

                addCustomerButton.IsEnabled = false;
                await CustomerDatabaseViewModel.Instance().SaveItem(customer);
                ClearCustomerForm();
                saveResult.Text = "Zákazník byl přidán.";
                addCustomerButton.IsEnabled = true;

                customerObsColl.Add(customer);
                ClosePopups();
            }
            else
            {
                saveResult.Text = "Vyplňte správně formulář.";
            }
        }
        private async void addDrugButton_Click(object sender, RoutedEventArgs e)
        {
            numCheck1 = int.TryParse(drugNameInput.Text, out num);
            numCheck2 = int.TryParse(drugOEMInput.Text, out num);
            numCheck3 = int.TryParse(drugCompositionInput.Text, out num);
            numCheck4 = int.TryParse(drugStockInput.Text, out num);
            if (CheckInputsForInts() && numCheck1.Equals(false) && numCheck2.Equals(false) && numCheck3 && numCheck4)
            {
                drug = new Drug();
                stock = new Stock();

                drug.Name = drugNameInput.Text;
                drug.Distributor = drugOEMInput.Text;
                drug.Cost = Convert.ToInt32(drugCompositionInput.Text);
                drug.Type = drugType.Text;
                drug.Added = DateTime.Now;
                drug.Composition = "Allergen";

                stock.Amount = Convert.ToInt32(drugStockInput.Text);
                stock.Name = drugNameInput.Text;

                addDrugButton.IsEnabled = false;
                await DrugDatabaseViewModel.Instance().SaveItem(drug);
                await StockDatabaseViewModel.Instance().SaveItem(stock);
                ClearDrugForm();
                saveResult2.Text = "Lék byl přidán.";
                addDrugButton.IsEnabled = true;

                drugObsColl.Add(drug);
                ClosePopups();
            }
            else
            {
                saveResult2.Text = "Vyplňte správně formulář.";
            }
        }

        //Deleting methods
        private async void deleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            deleteCustomerButton.IsEnabled = false;
            await CustomerDatabaseViewModel.Instance().DeleteItem(customerListView.SelectedItem as Customer);
            deleteCustomerButton.IsEnabled = true;

            customerObsColl.Remove(customerListView.SelectedItem as Customer);
            DataToPurchasingDrugs();
            ClosePopups();
        }
        private async void deleteDrugButton_Click(object sender, RoutedEventArgs e)
        {
            deleteDrugButton.IsEnabled = false;
            var tempObj = drugListView.SelectedItem as Drug;
            await DrugDatabaseViewModel.Instance().DeleteItem(drugListView.SelectedItem as Drug);
            //await StockDatabaseViewModel.Instance().DeleteItem(StockDatabaseViewModel.Instance().GetItem(tempObj.ID).Result);
            deleteDrugButton.IsEnabled = true;

            drugObsColl.Remove(tempObj);
            ClosePopups();
        }
        //Updating methods
        private async void changeCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            customerListView.SelectedIndex = tempSelectedIndex;
            var tempObj = customerListView.SelectedItem as Customer;
            if (tempObj != null)
            {
                tempObj.Allergens = selectedCustomerFormAllergies.Text;
                tempObj.Name = selectedCustomerFormName.Text;
                changeCustomerButton.IsEnabled = false;
                await CustomerDatabaseViewModel.Instance().SaveItem(tempObj);
                customerObsColl = new ObservableCollection<Customer>(
                    CustomerDatabaseViewModel.Instance().LoadData().OrderBy(i => i.ID)
                );
                customerListView.ItemsSource = customerObsColl;

                saveResult3.Text = "Informace byly aktualizovány.";
                changeCustomerButton.IsEnabled = true;

                customerListView.SelectedIndex = tempSelectedIndex;
                DisplayCustomerData(tempObj);
            }
        }
        private async void changeDrugButton_Click(object sender, RoutedEventArgs e)
        {
            drugListView.SelectedIndex = tempSelectedIndex;
            var tempObj = drugObsColl[drugListView.SelectedIndex];
            if (tempObj != null && int.TryParse(selectedDrugFormStock.Text, out int num) && ChangeDrugPopupFormValidation())
            {
                stock = StockDatabaseViewModel.Instance().GetDrugStock(tempObj.Name);
                stock.Name = selectedDrugFormName.Text;
                stock.Amount = Convert.ToInt32(selectedDrugFormStock.Text);
                await StockDatabaseViewModel.Instance().SaveItem(stock);

                tempObj.Name = selectedDrugFormName.Text;
                tempObj.Distributor = selectedDrugFormDistributor.Text;
                tempObj.Cost = Convert.ToInt32(selectedDrugFormComposition.Text);
                tempObj.Type = selectedDrugFormType.Text;

                updateDrugButton.IsEnabled = false;
                await DrugDatabaseViewModel.Instance().SaveItem(tempObj);
                drugObsColl = new ObservableCollection<Drug>(
                    DrugDatabaseViewModel.Instance().LoadData().OrderBy(i => i.ID)
                );
                drugListView.ItemsSource = drugObsColl;

                saveResult4.Text = "Informace byly aktualizovány.";
                updateDrugButton.IsEnabled = true;

                drugListView.SelectedIndex = tempSelectedIndex;
                DisplayDrugData(tempObj);
            }
        }
        private async void purchaseButton_Click(object sender, RoutedEventArgs e)
        {
            //Selected drug and customer.
            if (selectDrug.SelectedItem != null && selectCustomer.SelectedItem != null)
            {
                var tempObj1 = selectCustomer.SelectedItem as Customer;
                MessageBoxButton buttons = MessageBoxButton.YesNo;
                int sum = 0;
                foreach (Drug item in selectDrug.SelectedItems)
                {
                    sum += item.Cost;
                }
                    
                var res = MessageBox.Show($"Chcete potvrdit nákup za {sum},- Kč?", "Potvrzení nákupu", buttons);
                if (res.Equals(MessageBoxResult.Yes))
                {
                    order = new Order();
                    foreach (Drug item in selectDrug.SelectedItems)
                    {
                        order.Date = DateTime.Now;
                        order.CustomerID = tempObj1.ID;
                        order.PurchasedItem = item.Name;

                        await OrderDatabaseViewModel.Instance().SaveItem(order);
                    }
                    MessageBox.Show("Nákup byl potvrzen.");
                    var orderJoin = from customersDatabase in CustomerDatabaseViewModel.Instance().LoadData()
                                    join ordersDatabase in OrderDatabaseViewModel.Instance().LoadData() on customersDatabase.ID equals ordersDatabase.CustomerID
                                    join drugs in DrugDatabaseViewModel.Instance().LoadData() on ordersDatabase.PurchasedItem equals drugs.Name
                                    select new { CustomerName = customersDatabase.Name, OrderID = ordersDatabase.ID, OrderDate = ordersDatabase.Date };
                    orderListView.ItemsSource = orderJoin;
                    selectDrug.SelectedIndex = -1;
                    selectCustomer.SelectedIndex = -1;
                }
            }
        }

        //Selection changed methods
        private void customerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (customerListView.SelectedItem != null)
            {
                var tempObj = customerListView.SelectedItem as Customer;
                tempSelectedIndex = customerListView.SelectedIndex;
                customerOverviewPopup.IsOpen = true;
                OpenPopup();
                
                DisplayCustomerData(tempObj);
            }
        }
        private void drugList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (drugListView.SelectedItem != null)
            {
                tempSelectedIndex = drugListView.SelectedIndex;
                drugOverviewPopup.IsOpen = true;
                OpenPopup();
                var tempObj = drugListView.SelectedItem as Drug;
                DisplayDrugData(tempObj);
            }
        }

        //Popup methods
        private void closePopup_Click(object sender, RoutedEventArgs e)
        {
            ClosePopups();
        }
        private void displayCustomerPopup(object sender, RoutedEventArgs e)
        {
            addCustomerPopup.IsOpen = true;
            OpenPopup();
        }
        private void displayDrugPopup(object sender, RoutedEventArgs e)
        {
            addDrugPopup.IsOpen = true;
            OpenPopup();
        }

        //Other methods
        private void DataToPurchasingDrugs()
        {
            selectCustomer.ItemsSource = customerObsColl;
            selectDrug.ItemsSource = drugObsColl;
        }
        private void ClosePopups()
        {
            content.IsEnabled = true;
            blEffect.Radius = 0;
            addCustomerPopup.IsOpen = false;
            addDrugPopup.IsOpen = false;
            customerOverviewPopup.IsOpen = false;
            drugOverviewPopup.IsOpen = false;
            saveResult.Text = "";
            saveResult2.Text = "";
            saveResult3.Text = "";
            customerListView.SelectedIndex = -1;
            drugListView.SelectedIndex = -1;
        }
        private void ClearCustomerForm()
        {
            userNameInput.Text = "";
            userAllergensInput.Text = "";
        }
        private void ClearDrugForm()
        {
            drugNameInput.Text = "";
            drugOEMInput.Text = "";
            drugCompositionInput.Text = "";
            drugStockInput.Text = "";
        }
        private bool CheckInputsForInts()
        {
            if (string.IsNullOrEmpty(drugNameInput.Text).Equals(false) && 
                string.IsNullOrEmpty(drugOEMInput.Text).Equals(false) && 
                string.IsNullOrEmpty(drugCompositionInput.Text).Equals(false) && 
                string.IsNullOrEmpty(drugStockInput.Text).Equals(false))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ChangeDrugPopupFormValidation()
        {
            if (string.IsNullOrEmpty(selectedDrugFormName.Text).Equals(false) &&
                string.IsNullOrEmpty(selectedDrugFormDistributor.Text).Equals(false) &&
                string.IsNullOrEmpty(selectedDrugFormComposition.Text).Equals(false) &&
                string.IsNullOrEmpty(selectedDrugFormStock.Text).Equals(false) &&
                selectedDrugFormType.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void OpenPopup()
        {
            blEffect.Radius = 4;
            content.IsEnabled = false;
        }

        //--Methods for displaying data in overview popup windows-- 
        private void DisplayCustomerData(Customer tempObj)
        {
            CustomerViewModel.Instance().customer = tempObj;
            selectedPersonDOB.Text = $"Datum narození: {tempObj.DateOfBirth.Split(' ')[0]} (Věk: {CustomerViewModel.Instance().GetCustomerAge()})\nAlergeny: {tempObj.Allergens}";
            customerOrdersList.ItemsSource = OrderDatabaseViewModel.Instance().LoadOrders(tempObj.ID);
            selectedCustomerName.Text = tempObj.Name;
            selectedCustomerFormName.Text = tempObj.Name;
            selectedCustomerFormAllergies.Text = tempObj.Allergens;
        }
        private void DisplayDrugData(Drug tempObj)
        {
            selectedDrugComposition.Text = "";
            selectedDrugComposition.Text = $"Cena: {tempObj.Cost}";
            selectedDrugName.Text = tempObj.Name;
            selectedDrugData.Text = $"Distributor: {tempObj.Distributor}\nTyp: {tempObj.Type}\nPočet: {StockDatabaseViewModel.Instance().GetDrugStock(tempObj.Name).Amount}";

            selectedDrugFormName.Text = tempObj.Name;
            selectedDrugFormComposition.Text = $"{tempObj.Cost}";
            selectedDrugFormDistributor.Text = tempObj.Distributor;
            selectedDrugFormType.Text = tempObj.Type;
            selectedDrugFormStock.Text = $"{StockDatabaseViewModel.Instance().GetDrugStock(tempObj.Name).Amount}";
        }

        private void selectDrug_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (selectDrug.SelectedItem != null && selectCustomer.SelectedItem != null)
            {
                var tempDrug = selectDrug.SelectedItem as Drug;
                var tempCustomer = selectCustomer.SelectedItem as Customer;

                if (tempCustomer.Allergens.Contains(tempDrug.Composition))
                {
                    MessageBox.Show($"Na {tempDrug.Name} je zvolený zákazník alergický.");
                }
            }
        }
    }
}
