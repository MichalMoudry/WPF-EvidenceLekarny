﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Wpf_lekarna.Interfaces
{
    public interface DatabaseTemplate
    {
        [PrimaryKey, AutoIncrement, Indexed]
        int ID
        {
            get;set;
        }
        string Name
        {
            get;set;
        }
    }
}
