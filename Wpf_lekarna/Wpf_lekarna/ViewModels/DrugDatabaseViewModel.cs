﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;

namespace Wpf_lekarna.ViewModels
{
    class DrugDatabaseViewModel : Interfaces.IDatabaseViewModel<Drug>
    {
        private static DrugDatabaseViewModel _instance;
        public static DrugDatabaseViewModel Instance()
        {
            if (_instance == null)
            {
                _instance = new DrugDatabaseViewModel();
            }
            return _instance;
        }

        private static DrugDatabase _drugDatabase;
        private static DrugDatabase DrugDatabase
        {
            get
            {
                if (_drugDatabase == null)
                {
                    var fileHelper = new Services.FileHelper();
                    _drugDatabase = new DrugDatabase(fileHelper.GetLocalFilePath("drugDatabase.db3"));
                }
                return _drugDatabase;
            }
        }
        
        public async Task SaveItem(Drug item)
        {
            await DrugDatabase.SaveItemAsync(item);
        }

        public List<Drug> LoadData()
        {
            return DrugDatabase.GetItemsAsync<Drug>().Result;
        }

        public async Task DeleteItem(Drug item)
        {
            await DrugDatabase.DeleteItemAsync(item);
        }

        public async Task<Drug> GetItem(int id)
        {
            var temp = await DrugDatabase.GetItemAsync<Drug>(id);
            return temp;
        }
    }
}
