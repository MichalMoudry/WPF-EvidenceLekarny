﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;

namespace Wpf_lekarna.ViewModels
{
    class OrderDatabaseViewModel : Interfaces.IDatabaseViewModel<Order>
    {
        private static OrderDatabaseViewModel _instance;
        public static OrderDatabaseViewModel Instance()
        {
            if (_instance == null)
            {
                _instance = new OrderDatabaseViewModel();
            }
            return _instance;
        }

        private static OrderDatabase _ordersDB;
        private static OrderDatabase OrdersDB
        {
            get
            {
                if (_ordersDB == null)
                {
                    var fileHelper = new Services.FileHelper();
                    _ordersDB = new OrderDatabase(fileHelper.GetLocalFilePath("ordersDatabase.db3"));
                }
                return _ordersDB;
            }
        }

        public async Task SaveItem(Order item)
        {
            await OrdersDB.SaveItemAsync(item);
        }

        public List<Order> LoadData()
        {
            return OrdersDB.GetItemsAsync<Order>().Result;
        }

        public async Task DeleteItem(Order item)
        {
            await OrdersDB.DeleteItemAsync(item);
        }

        public async Task<Order> GetItem(int id)
        {
            var temp = await OrdersDB.GetItemAsync(id);
            return temp;
        }

        public List<Order> LoadOrders(int customerID)
        {
            return OrdersDB.GetCustomerOrders(customerID).Result;
        }
    }
}
