﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Wpf_lekarna.Models
{
    public class Drug : Interfaces.DatabaseTemplate
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        //Name of drug
        private string _name; 
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        //Name of distributor
        private string _distributor; 
        public string Distributor
        {
            get { return _distributor; }
            set { _distributor = value; }
        }

        private string _composition;
        public string Composition
        {
            get { return _composition; }
            set { _composition = value; }
        }

        private string _type;   
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private int _cost;
        public int Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }


        private DateTime _added;
        public DateTime Added
        {
            get { return _added; }
            set { _added = value; }
        }

    }
}
