﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;

namespace Wpf_lekarna.ViewModels
{
    public class CustomerViewModel
    {
        private static CustomerViewModel _instance;
        public static CustomerViewModel Instance()
        {
            if (_instance == null)
            {
                _instance = new CustomerViewModel();
            }
            return _instance;
        }

        public Customer customer;
        public double GetCustomerAge()
        {
            if (customer != null)
            {
                DateTime today = DateTime.Now;
                DateTime customerDOB = Convert.ToDateTime(customer.DateOfBirth);
                return Math.Round(((today - customerDOB).TotalDays / 360), 1);
            }
            else
            {
                return 0;
            }
        }
    }
}
