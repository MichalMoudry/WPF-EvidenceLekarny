﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;   
using SQLite;

namespace Wpf_lekarna.Models
{
    public class CustomerDatabase
    {               
        //Connection    
        private SQLiteAsyncConnection database; 
        public CustomerDatabase(string dbPath)   
        {   
            database = new SQLiteAsyncConnection(dbPath); 
            database.CreateTableAsync<Customer>().Wait(); 
        }  

        //Get table in List   
        public Task<List<T>> GetItemsAsync<T>() where T : new() 
        {     
            return database.Table<T>().ToListAsync(); 
        }  

        //Get class instance ffrom database 
        public Task<T> GetItemAsync<T>(int id) where T : class, Interfaces.DatabaseTemplate, new()
        {       
            return database.Table<T>().Where(i => i.ID == id).FirstOrDefaultAsync(); 
        }  

        //Inserting/updating data to database 
        public Task<int> SaveItemAsync<T>(T item) where T : class, Interfaces.DatabaseTemplate,new () 
        {      
            if (item.ID != 0)  
            {       
                return database.UpdateAsync(item);   
            }  
            else   
            {       
                return database.InsertAsync(item); 
            }  
        } 

        //Deleting 
        public Task<int> DeleteItemAsync<T>(T item) where T : new () 
        {     
            return database.DeleteAsync(item); 
        }

        //Searching
        public Task<T> SearchForItem<T>(string query) where T : class, Interfaces.DatabaseTemplate, new()
        {
            return database.Table<T>().Where(i => i.Name.Contains(query)).FirstOrDefaultAsync();
        }
    }
}
