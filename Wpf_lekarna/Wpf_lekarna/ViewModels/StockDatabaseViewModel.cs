﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_lekarna.Models;

namespace Wpf_lekarna.ViewModels
{
    class StockDatabaseViewModel : Interfaces.IDatabaseViewModel<Stock>
    {
        private static StockDatabaseViewModel _instance;
        public static StockDatabaseViewModel Instance()
        {
            if (_instance == null)
            {
                _instance = new StockDatabaseViewModel();
            }
            return _instance;
        }

        private static StockDatabase _stockDatabase;
        private static StockDatabase StockDatabase
        {
            get
            {
                if (_stockDatabase == null)
                {
                    var fileHelper = new Services.FileHelper();
                    _stockDatabase = new StockDatabase(fileHelper.GetLocalFilePath("stockDatabase.db3"));
                }
                return _stockDatabase;
            }
        }

        public async Task SaveItem(Stock item)
        {
            await StockDatabase.SaveItemAsync(item);
        }

        public List<Stock> LoadData()
        {
            return StockDatabase.GetItemsAsync<Stock>().Result;
        }

        public async Task DeleteItem(Stock item)
        {
            await StockDatabase.DeleteItemAsync(item);
        }

        public async Task<Stock> GetItem(int id)
        {
            var temp = await StockDatabase.GetItemAsync<Stock>(id);
            return temp;
        }

        public Stock GetDrugStock(string itemName)
        {
            return StockDatabase.GetItemStock(itemName).Result;
        }
    }
}
