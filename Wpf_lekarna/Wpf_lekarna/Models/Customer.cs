﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Wpf_lekarna.Models
{
    public class Customer : Interfaces.DatabaseTemplate
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        //Name of customer
        private string _name; 
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _allergens;
        public string Allergens
        {
            get { return _allergens; }
            set { _allergens = value; }
        }

        private string _dateOfBirth;   
        public string DateOfBirth
        {
            get { return _dateOfBirth.Split(' ')[0]; }
            set { _dateOfBirth = value; }
        }

        private DateTime _added;
        public DateTime Added
        {
            get { return _added; }
            set { _added = value; }
        }

    }
}
